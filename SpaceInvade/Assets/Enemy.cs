﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	// Use this for initialization
	public GameObject enmbullet;
	bool move;
	float slide;
	float rdmattk;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		Vector3 enmy = this.transform.position;
		if (move == true) {
			enmy.x += 5 * Time.deltaTime;
			slide += 5 * Time.deltaTime;
			if (slide >= 7) {
				move = false;
				enmy.y -= 5;
				slide = 0;
			}
		} else if (move == false) {
			enmy.x -= 5 * Time.deltaTime;
			slide += 5 * Time.deltaTime;
			if (slide >= 7) {
				move = true;
				enmy.y -= 5;
				slide = 0;
			}
		}
		this.transform.position = enmy;

		rdmattk += Time.deltaTime;
		if (rdmattk >= 1f) {
			int att = Random.Range (0, 10);
			if (att == 0) {
				Vector3 bull = this.transform.position;
				bull.y -= 3;
				Instantiate (enmbullet,bull, Quaternion.identity);
			}
			rdmattk = 0;
		}
	}
}




