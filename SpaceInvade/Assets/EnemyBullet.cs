﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		Vector3 shoot = this.transform.position;
		shoot.y -= 15f * Time.deltaTime;
		this.transform.position = shoot;
	}

	void OnCollisionEnter (Collision other){
		if (other.gameObject.tag != "Enemy") {
			Destroy (other.gameObject);
			Destroy (this.gameObject);
		}
	}
}